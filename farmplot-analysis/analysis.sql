--
--   Copyright 2022 IIT Bombay
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
-- This query uses the following tables:
--
--     "pocra_dashboard"."Village":
--         village polygons expressed in lat-long (EPSG:4326)
--
--     "public"."google_cadastres_postgis":
--         farm plots data provided by Google in GeoJSON format was
--         loaded as PostGIS polygon geometry using
--         ST_GeomFromGeoJSON() function.  The polygon coordinates are
--         assumed to be lat-long (EPSG:4326)
--
--     "osmanabad"."cadastres":
--         cadastres (farm plots) from the Maharasthra state land
--         ownership records.  Polygon geometry projected using UTM
--         EPSG:32643 coordinate system.
--
-- Objective is to compare the state cadastral data with farm plots
-- identified by Google.  The query starts by definiting two
-- subqueries followed by the main query.  Inline comments are added
-- to help explain the intent further.
--
with covered_villages as (
    -- Subquery: villages polygons completely contained in the
    -- bounding box of Google farm plots.
    select
        v.*
    from
        google_osmanabad."villages" v
    inner join
	(select
	     st_envelope(st_union(st_envelope(g.geom))) as geom
	 from
	     google_cadastres_postgis g) gc
    on
        st_contains(gc.geom, v.geom)
),

intersections as (
    -- Subquery: Google farm plots intersection with village polygons
    select
        lower(v.dtnname) as district,
	lower(v.thnname) as taluka,
	lower(v.vilname) as village,
        count(*) as num_intersecting_farmplots,
        sum(
	    st_area(
	        st_intersection(
                    -- Transform the lat-long geometries to a flat
                    -- surface in 2 dimensions using a suitable
                    -- projection.  Geometric operations such as area,
                    -- perimeter, distance are easier to comprehend in
                    -- 2D.  The EPSG:32643 coordinate system projects
                    -- lat-long from the region that covers central
                    -- Maharashtra to 2D.  See
                    -- https://epsg.org/crs_32643/WGS-84-UTM-zone-43N.html.
                    st_transform(gc.geom, 32643),
		    st_transform(v.geom, 32643)
		)
            )
	) as intersecting_farmplots_area,
	st_area(st_transform(v.geom, 32643)) as village_area
    from
        covered_villages v
    inner join
        google_cadastres_postgis gc
    on
	st_intersects(v.geom, gc.geom)
    and
        v.dtnname = 'Osmanabad'
    group by
        v.dtnname, v.thnname, v.vilname, v.geom),

cadastres as (
    -- Village-wise summary of the state cadastral data.
    select
        lower(c.d_name) as district,
	lower(c.t_name) as taluka,
	lower(c.location) as village,
	count(*) as num_cadastres,
	-- Coordinate transformation not needed because this data is
	-- already projected in 2D using EPSG:32643.
	sum(st_area(c.geom)) as cadastral_area
    from
        osmanabad.cadastres c
    group by
        -- This aggregation causes the results to be village-wise (the
        -- "location" column from the state cadastral data contains
        -- village name).
        c.d_name, c.t_name, c.location)

-- Main query.  Join the results from the two subqueries defined
-- above.
select
    i.district, i.taluka, i.village,
    i.intersecting_farmplots_area::numeric(10, 0) as total_farmplot_area,
    i.village_area::numeric(10, 0),
    (100 * i.intersecting_farmplots_area / i.village_area)
        ::numeric(10, 2) as "farmplot_coverage %",
    i.num_intersecting_farmplots as intersecting_farmplots,
    c.num_cadastres,
    c.cadastral_area::numeric(10, 0)
from
    intersections i
inner join
    cadastres c
using
    (district, taluka, village)
order by
    "farmplot_coverage %"
;
