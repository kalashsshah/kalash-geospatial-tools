#!/bin/sh
#
#   Copyright 2022 IIT Bombay
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Script to load GeoJSON data into PostGIS.  This is done by replacing
# "{path_to_geojson_csv}" marker from the
# ingest_geojson_farmplots.sql.in with user specified path.  And
# running the resulting SQL file using psql.
#
# Assumptions:
#
#     1. POSIX shell environment (Linux, MacOS, FreeBSD, etc.).
#
#     2. PostgreSQL server is running on localhost with default port
#        or PGPORT environment variable is set properly.
#
#     3. Current user can connect to PostgreSQL without having to
#        authenticate.

USAGE="./load_geojson_to_postgis.sh <path_to_geojson_csv> <dbname>"

GEOJSON_CSV=$1
DBNAME=$2

if [ "X$GEOJSON_CSV" == "X" ];
then
    echo "path to GeoJSON CSV file name must be specified\n$USAGE"
    exit 1
fi
if [ "X$DBNAME" == "X" ];
then
    echo "database name must be specified\n$USAGE"
    exit 1
fi

# Use "%" character as delimiter in the sed command.  Typically, "/"
# is used as the delimiter in sed commands.  In our case, the
# GEOJSON_CSV is a path, which may contain several "/" characters.
# Using a different delimiter allows using the user specified path
# as-is, without having to escape the "/".
sed -e s%{path_to_geojson_csv}%$GEOJSON_CSV% \
    ingest_geojson_farmplots.sql.in > ingest_geojson_farmplots.sql
if [ $? -ne 0 ];
then
    echo "sed failed with exit status $?"
    exit 1
fi

psql -d $DBNAME -f ingest_geojson_farmplots.sql -e
if [ $? -ne 0 ];
then
    echo "psql failed with exit status $?"
    exit 1
fi

echo "GeoJSON data from $GEOJSON_CSV loaded successfully"
exit 0
